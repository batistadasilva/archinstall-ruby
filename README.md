# ArchRy

ArchRy (pronounced 'archery') is an [Arch Linux](https://wiki.archlinux.org/title/Arch_Linux) installer that allows you
to describe your whole system in a simple YAML file.

## Features

* Full-system encryption ([LVM on LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS))
* Both BIOS and UEFI support
* Automate installation of official packages, AUR packages, as well as your own builds
* ...and much more

## Usage (TL; DR)

### From a Live ISO Image

Connect to the internet

```sh
iwctl
```

Update the system clock

```sh
timedatectl set-ntp true
```

Download & extract

```sh
curl https://gitlab.com/batistadasilva/archry/-/archive/main/archry-main.tar.gz | tar -xz
```

Run

```sh
cd archry-main
./run --config </path/to/config/file>
```

## Documentation

See a configuration file example [here](docs/sample-config.yml).

_More documentation coming soon..._

## License

Licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
