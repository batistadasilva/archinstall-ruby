# frozen_string_literal: true

require 'singleton'

module ArchRy
  module AccessControl
    # Superuser is a special user account used for system administration. Depending on the operating system, the actual
    # name of this account might be root, administrator, admin or supervisor.
    #
    # See https://en.wikipedia.org/wiki/Superuser
    class Superuser
      include Singleton

      def username
        'root'
      end

      def password
        Password.new(user: self)
      end
    end
  end
end
