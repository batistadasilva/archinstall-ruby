# frozen_string_literal: true

module ArchRy
  module AccessControl
    # A user is anyone who uses a computer. In this case, we are describing the names which represent those users. It
    # may be Mary or Bill, and they may use the names Dragonlady or Pirate in place of their real name. All that matters
    # is that the computer has a name for each account it creates, and it is this name by which a person gains access to
    # use the computer.
    #
    # See https://wiki.archlinux.org/title/Users_and_groups
    class User
      attr_reader :username, :full_name

      def initialize(username:, full_name:)
        @username = username
        @full_name = full_name
      end

      def create!(groups: nil)
        if groups
          TTY::Command.default.run(
            :useradd, '--create-home', '--groups', groups.join(','), '--comment', full_name, username
          )
        else
          TTY::Command.default.run :useradd, '--create-home', '--comment', full_name, username
        end
      end

      def password
        Password.new(user: self)
      end
    end
  end
end
