# frozen_string_literal: true

module ArchRy
  module Booting
    module BootLoader
      # BIOS (Basic Input/Output System) is a firmware used to provide runtime services for operating systems and programs
      # and to perform hardware initialization during the booting process (power-on startup).[1] The BIOS firmware comes
      # pre-installed on an IBM PC or IBM PC compatible's system board and exists in UEFI-based systems too.
      #
      # See https://en.wikipedia.org/wiki/BIOS
      class BIOS
        attr_reader :disk

        def initialize(disk:)
          @disk = disk
        end

        def install!
          TTY::Command.default.run :'grub-install', '--target=i386-pc', '--recheck', disk.path
        end
      end
    end
  end
end
