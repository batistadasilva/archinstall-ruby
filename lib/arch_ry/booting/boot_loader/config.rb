# frozen_string_literal: true

require 'singleton'

module ArchRy
  module Booting
    module BootLoader
      class Config
        DEFAULTS_FILEPATH = '/etc/default/grub'
        TARGET_FILEPATH = '/boot/grub/grub.cfg'

        attr_reader :root_device, :crypt_container, :crypt_keyfile

        def initialize(root_device:, crypt_container: nil, crypt_keyfile: nil)
          @root_device = root_device
          @crypt_container = crypt_container
          @crypt_keyfile = crypt_keyfile
        end

        def create!
          grub_cmdline_linux = "root=#{root_device.path}"
          if crypt_container
            grub_cmdline_linux.prepend("cryptdevice=#{crypt_container.device.path}:#{crypt_container.name} ")
          end
          grub_cmdline_linux << " cryptkey=rootfs:#{crypt_keyfile}" if crypt_keyfile
          TTY::Command.default.run :mkdir, '-p', '/boot/grub'
          TTY::Command.default.run :sed, '--in-place', '/GRUB_ENABLE_CRYPTODISK/s/^#//', DEFAULTS_FILEPATH
          TTY::Command.default.run :sed, '--in-place',
                                   "/^GRUB_CMDLINE_LINUX=/c\GRUB_CMDLINE_LINUX=\"#{grub_cmdline_linux}\"", DEFAULTS_FILEPATH
          TTY::Command.default.run :'grub-mkconfig', '-o', TARGET_FILEPATH
          TTY::Command.default.run :chmod, '700', '/boot'
        end
      end
    end
  end
end
