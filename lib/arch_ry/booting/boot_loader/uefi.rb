# frozen_string_literal: true

module ArchRy
  module Booting
    module BootLoader
      # The Unified Extensible Firmware Interface (UEFI or EFI for short) is a model for the interface between operating
      # systems and firmware. It provides a standard environment for booting an operating system and running pre-boot
      # applications.
      #
      # See https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface
      class UEFI
        attr_reader :id, :directory

        def initialize(id: 'Arch Linux', directory: '/efi')
          @id = id
          @directory = directory
        end

        def install!
          TTY::Command.default.run :'grub-install', '--target=x86_64-efi',
                                   "--efi-directory=#{directory}", "--bootloader-id=#{id}", '--recheck'
        end
      end
    end
  end
end
