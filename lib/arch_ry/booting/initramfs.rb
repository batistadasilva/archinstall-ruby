# frozen_string_literal: true

require 'arch_ry/booting/initramfs/config'
require 'arch_ry/booting/initramfs/image'

module ArchRy
  module Booting
    # In Linux systems, initrd (initial ramdisk) is a scheme for loading a temporary root file system into memory, to be
    # used as part of the Linux startup process. initrd and initramfs refer to two different methods of achieving this.
    # Both are commonly used to make preparations before the real root file system can be mounted.
    #
    # See https://en.wikipedia.org/wiki/Initial_ramdisk
    module Initramfs
    end
  end
end
