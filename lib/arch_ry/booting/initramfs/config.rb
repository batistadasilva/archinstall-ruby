# frozen_string_literal: true

require 'singleton'

module ArchRy
  module Booting
    module Initramfs
      class Config
        include Singleton

        FILEPATH = '/etc/mkinitcpio.conf'

        def update!(**values)
          update_hooks!(values[:hooks]) if values.key?(:hooks)
          update_files!(values[:files]) if values.key?(:files)
        end

        private

        def update_hooks!(hooks)
          TTY::Command.default.run :sed, '--in-place', "/^HOOKS=/c\HOOKS=(#{Array(hooks).join(' ')})", FILEPATH
        end

        def update_files!(files)
          TTY::Command.default.run :sed, '--in-place', "/^FILES=/c\FILES=(#{Array(files).join(' ')})", FILEPATH
        end
      end
    end
  end
end
