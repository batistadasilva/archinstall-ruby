# frozen_string_literal: true

module ArchRy
  module Booting
    module Initramfs
      class Image
        attr_reader :preset

        def initialize(preset: nil)
          @preset = preset
        end

        def create!
          if preset
            TTY::Command.default.run :mkinitcpio, '--preset', preset
          else
            TTY::Command.default.run :mkinitcpio, '--allpresets'
          end
        end
      end
    end
  end
end
