# frozen_string_literal: true

require 'arch_ry/filesystem/lvm'
require 'arch_ry/filesystem/luks'
require 'arch_ry/filesystem/block_device'
require 'arch_ry/filesystem/lvm/logical_volume'
require 'arch_ry/filesystem/lvm/physical_volume'
require 'arch_ry/filesystem/lvm/volume_group'
require 'arch_ry/filesystem/luks/container'
require 'arch_ry/filesystem/disk'
require 'arch_ry/filesystem/partition_table'
require 'arch_ry/filesystem/partition'
require 'arch_ry/filesystem/fstab'

module ArchRy
  # Filesystem (often abbreviated to fs) is a method and data structure that the operating system uses to control how
  # data is stored and retrieved.
  #
  # See https://en.wikipedia.org/wiki/File_system
  module Filesystem
  end
end
