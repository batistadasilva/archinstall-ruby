# frozen_string_literal: true

require 'json'

module ArchRy
  module Filesystem
    # Block devices provide buffered access to hardware devices, and provide some abstraction from their specifics.
    #
    # See https://en.wikipedia.org/wiki/Device_file#Block_devices
    class BlockDevice
      include LUKS
      include LVM

      attr_reader :name

      class << self
        def lsblk
          result = TTY::Command.quiet.run(:lsblk, '--json', '--output', 'NAME,MODEL,PATH,SIZE,TYPE,PARTTYPENAME,UUID')
          JSON.parse(result.out)['blockdevices']
        end

        def from_lsblk(json)
          raise NotImplementedError
        end
      end

      def mount!(mountpoint)
        if mountpoint == :swap
          TTY::Command.default.run :swapon, path
        else
          TTY::Command.default.run :mkdir, '-p', mountpoint
          TTY::Command.default.run :mount, path, mountpoint
        end
      end

      def wipe!(mode = :default)
        case mode
        when :default
          fill_with_random_bytes!
        when :fast
          fill_with_zeros!
        else
          raise "Unsupporter wiping mode: #{mode}"
        end
      end

      def format!(type)
        case type
        when :fat32
          TTY::Command.default.run :'mkfs.fat', '-F32', path
        when :ext4
          TTY::Command.default.run :'mkfs.ext4', path
        when :swap
          TTY::Command.default.run :mkswap, path
        else
          raise "Unsupported filesystem type: #{type}"
        end
      end

      private

      def fill_with_zeros!
        TTY::Command.default.run! :dd, 'if=/dev/zero', "of=#{path}", 'bs=4M', 'status=progress'
      end

      def fill_with_random_bytes!
        TTY::Command.default.run! :dd, 'if=/dev/urandom', "of=#{path}", 'bs=4M', 'status=progress'
      end
    end
  end
end
