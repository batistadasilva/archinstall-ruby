# frozen_string_literal: true

module ArchRy
  module Filesystem
    # A hard disk drive (HDD), hard disk, hard drive, or fixed disk is an electro-mechanical data storage device that
    # stores and retrieves digital data using magnetic storage and one or more rigid rapidly rotating platters coated
    # with magnetic material.
    #
    # See https://en.wikipedia.org/wiki/Hard_disk_drive
    class Disk < BlockDevice
      class << self
        def all
          disk_devices = lsblk.select { |device| device['type'] == 'disk' }
          disk_devices.map { |device| from_lsblk(device) }
        end

        def from_lsblk(json)
          new(name: json['name'], path: json['path'], model: json['model'])
        end
      end

      attr_reader :path, :model

      def initialize(name:, path:, model:)
        super()
        @name = name
        @path = path
        @model = model
      end

      def partitions
        disk_lsblk = self.class.lsblk.detect { |blk| blk['path'] == path }
        return [] if disk_lsblk['children'].nil?

        disk_lsblk['children'].select { |child| child['type'] == 'part' }
                              .map { |child| Partition.from_lsblk(child) }
      end
    end
  end
end
