# frozen_string_literal: true

module ArchRy
  module Filesystem
    # The Linux Unified Key Setup (LUKS) is a disk encryption specification created by Clemens Fruhwirth in 2004
    # and was originally intended for Linux.
    #
    # See https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup
    module LUKS
      def encrypt!(passphrase)
        luks_container = Container.new(name: "#{name}_crypt", device: self)
        luks_container.create!(passphrase)
        luks_container
      end
    end
  end
end
