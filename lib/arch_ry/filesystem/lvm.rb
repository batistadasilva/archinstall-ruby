# frozen_string_literal: true

module ArchRy
  module Filesystem
    # Logical Volume Manager (LVM) is a device mapper framework that provides logical volume management for the Linux
    # kernel.
    #
    # See https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)
    module LVM
      def create_physical_volume!
        physical_volume = PhysicalVolume.new(device: self)
        physical_volume.create!
        physical_volume
      end

      def create_volume_group!(name)
        volume_group = VolumeGroup.new(name: name)
        volume_group.create!(self)
        volume_group
      end
    end
  end
end
