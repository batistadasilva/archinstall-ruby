# frozen_string_literal: true

module ArchRy
  module Filesystem
    module LVM
      # See https://en.wikipedia.org/wiki/Logical_volume_management
      class LogicalVolume < BlockDevice
        attr_reader :volume_group, :size

        def initialize(volume_group:, name:, size: nil)
          @name = name
          @size = size
          @volume_group = volume_group
        end

        def create!
          if size
            TTY::Command.default.run :lvcreate, '-L', size, volume_group.name, '-n', name
          else
            TTY::Command.default.run :lvcreate, '-l', '100%FREE', volume_group.name, '-n', name
          end
        end

        def path
          "/dev/#{volume_group.name}/#{name}"
        end
      end
    end
  end
end
