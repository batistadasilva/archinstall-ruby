# frozen_string_literal: true

module ArchRy
  module Filesystem
    module LVM
      # See https://en.wikipedia.org/wiki/Logical_volume_management
      class PhysicalVolume
        attr_reader :device

        def initialize(device:)
          @device = device
        end

        def create!
          TTY::Command.default.run :pvcreate, device.path
        end
      end
    end
  end
end
