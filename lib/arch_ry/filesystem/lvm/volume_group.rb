# frozen_string_literal: true

module ArchRy
  module Filesystem
    module LVM
      # See https://en.wikipedia.org/wiki/Logical_volume_management
      class VolumeGroup
        attr_reader :name

        def initialize(name:)
          @name = name
        end

        def create!(device)
          TTY::Command.default.run :vgcreate, name, device.path
        end

        def add_logical_volume!(name, size: nil)
          logical_volume = LogicalVolume.new(volume_group: self, name: name, size: size)
          logical_volume.create!
          logical_volume
        end
      end
    end
  end
end
