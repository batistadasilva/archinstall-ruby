# frozen_string_literal: true

module ArchRy
  module Filesystem
    # Disk partitioning or disk slicing is the creation of one or more regions on secondary storage, so that each
    # region can be managed separately. These regions are called partitions.
    #
    # See https://en.wikipedia.org/wiki/Disk_partitioning
    class Partition < BlockDevice
      class << self
        def from_lsblk(json)
          new(name: json['name'], path: json['path'], size: json['size'],
              type: from_partition_type_name(json['parttypename']))
        end

        private

        def from_partition_type_name(value)
          return nil if value.nil?

          case value.downcase
          when 'efi system'
            :efi
          when 'linux filesystem'
            :linux
          when 'bios boot'
            :bios
          else
            raise "Unsupported partition type: #{value}"
          end
        end
      end

      attr_reader :name, :path, :type, :size

      def initialize(name:, path:, type:, size:)
        super()
        @name = name
        @path = path
        @type = type
        @size = size
      end
    end
  end
end
