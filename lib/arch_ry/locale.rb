# frozen_string_literal: true

module ArchRy
  # Localization is the process of adapting internationalized software for a specific region or language by
  # translating text and adding locale-specific components. Localization (which is potentially performed multiple
  # times, for different locales) uses the infrastructure or flexibility provided by internationalization (which is
  # ideally performed only once before localization, or as an integral part of ongoing development).
  #
  # See https://en.wikipedia.org/wiki/Internationalization_and_localization
  #
  # Locales are used by glibc and other locale-aware programs or libraries for rendering text, correctly displaying
  # regional monetary values, time and date formats, alphabetic idiosyncrasies, and other locale-specific standards.
  #
  # See https://wiki.archlinux.org/title/Locale
  class Locale
    CONFIG_FILE = '/etc/locale.conf'
    GENERATED_FILE = '/etc/locale.gen'

    class << self
      def find_by(name:)
        available.detect { |l| l.name == name }
      end

      def current
        return nil unless TTY::Command.quiet.run!(:ls, CONFIG_FILE).success?

        current_value = TTY::Command.quiet.run(:cat, CONFIG_FILE).out.strip
        new(name: current_value)
      end

      def available
        lines = TTY::Command.quiet
                            .run(:cat, GENERATED_FILE)
                            .reject { |line| line.match?(/^#\s/) }
                            .map { |commented| commented.gsub(/^#/, '').split.first }
                            .map { |line| new(name: line) }
      end
    end

    attr_reader :name

    def initialize(name:)
      @name = name
    end

    def generate!
      TTY::Command.default.run! :sed, '--in-place', "/##{name}/s/^#//g", GENERATED_FILE
      TTY::Command.default.run :'locale-gen'
    end

    def set_as_current!
      TTY::Command.default.run(:echo, "LANG=#{name}", out: CONFIG_FILE)
    end
  end
end
