# frozen_string_literal: true

require 'arch_ry/networking/host'
require 'arch_ry/networking/hosts'
require 'arch_ry/networking/hostname'

module ArchRy
  # See https://wiki.archlinux.org/title/Network_configuration
  module Networking
    IPV4_LOCALHOST = '127.0.0.1'
    IPV6_LOCALHOST = '::1'
  end
end
