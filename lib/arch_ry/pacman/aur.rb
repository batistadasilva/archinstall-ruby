# frozen_string_literal: true

module ArchRy
  module Pacman
    # The Arch User Repository (AUR) is a community-driven repository for Arch users. It contains package descriptions
    # (PKGBUILDs) that allow you to compile a package from source with makepkg and then install it via pacman.
    #
    # See https://wiki.archlinux.org/title/Arch_User_Repository
    module AUR
      class Package
        attr_reader :name, :build_as, :pre_install, :post_install

        def initialize(name:, build_as:, pre_install: nil, post_install: nil)
          @name = name
          @build_as = build_as
          @post_install = post_install
          @pre_install = pre_install
        end

        def install!
          run_pre_install!
          download_and_extract!
          build_and_install!
          delete_source_folder!
          run_post_install!
        end

        private

        def download_and_extract!
          TTY::Command.default.run(
            :bash, '-c',
            "cd #{user_home} && curl -L https://aur.archlinux.org/cgit/aur.git/snapshot/#{name}.tar.gz | tar -xz",
            user: build_as
          )
        end

        def build_and_install!
          TTY::Command.default.run(
            :bash, '-c',
            "HOME=#{user_home} && cd #{source_folder} && makepkg -si --noconfirm",
            user: build_as
          )
        end

        def delete_source_folder!
          TTY::Command.default.run :rm, '-r', source_folder
        end

        def user_home
          "/home/#{build_as}"
        end

        def source_folder
          "#{user_home}/#{name}"
        end

        def run_pre_install!
          Array(pre_install).each do |command|
            TTY::Command.default.run :bash, '-c', command
          end
        end

        def run_post_install!
          Array(post_install).each do |command|
            TTY::Command.default.run :bash, '-c', command
          end
        end
      end
    end
  end
end
