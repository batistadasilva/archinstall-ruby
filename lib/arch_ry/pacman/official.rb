# frozen_string_literal: true

require 'arch_ry/pacman/official/installing'
require 'arch_ry/pacman/official/package'
require 'arch_ry/pacman/official/group'

module ArchRy
  module Pacman
    # Arch Linux official repositories contain essential and popular software, readily accessible via pacman. They are
    # maintained by package maintainers.
    #
    # Packages in the official repositories are constantly upgraded: when a package is upgraded, its old version is
    # removed from the repository. There are no major Arch releases: each package is upgraded as new versions become
    # available from upstream sources. Each repository is always coherent, i.e. the packages that it hosts always have
    # reciprocally compatible versions.
    #
    # See https://wiki.archlinux.org/title/Official_repositories
    module Official
      class << self
        # Minimal package set to define a basic Arch Linux installation
        #
        # See https://archlinux.org/packages/core/any/base/
        def base
          Package.new(name: 'base')
        end

        # See https://archlinux.org/groups/x86_64/base-devel/
        def base_devel
          Group.new(name: 'base-devel', post_install: "sed --in-place '/%wheel ALL=(.*) ALL/s/^# //' /etc/sudoers")
        end

        # The Linux kernel and modules
        #
        # See https://archlinux.org/packages/core/x86_64/linux/
        def linux(**options)
          Package.new(name: 'linux', **options)
        end

        # Firmware files for Linux
        #
        # See https://archlinux.org/packages/core/any/linux-firmware/
        def linux_firmware
          Package.new(name: 'linux-firmware')
        end

        # Logical Volume Manager 2 utilities
        #
        # See https://archlinux.org/packages/core/x86_64/lvm2/
        def lvm2
          Package.new(name: 'lvm2')
        end

        # GNU GRand Unified Bootloader (2)
        #
        # https://archlinux.org/packages/core/x86_64/grub/
        def grub
          Package.new(name: 'grub')
        end

        # Linux user-space application to modify the EFI Boot Manager
        #
        # See https://archlinux.org/packages/core/x86_64/efibootmgr/
        def efibootmgr
          Package.new(name: 'efibootmgr')
        end

        # Tools for manipulating UEFI secure boot platforms
        #
        # See https://archlinux.org/packages/extra/x86_64/efitools/
        def efitools
          Package.new(name: 'efitools')
        end

        # Tools to add signatures to EFI binaries and Drivers
        #
        # See https://archlinux.org/packages/extra/x86_64/sbsigntools/
        def sbsigntools
          Package.new(name: 'sbsigntools')
        end

        # Network Time Protocol reference implementation
        #
        # See https://archlinux.org/packages/extra/x86_64/ntp/
        def ntp
          Package.new(name: 'ntp')
        end
      end
    end
  end
end
