# frozen_string_literal: true

module ArchRy
  module Pacman
    module Official
      class Package
        include Official::Installing

        class << self
          def config_ignore_key
            'IgnorePkg'
          end

          def exists_command_option
            '-i'
          end
        end

        attr_reader :name, :version, :auto_update, :post_install, :force

        def initialize(name:, version: nil, auto_update: true, post_install: nil, force: false)
          @name = name
          @version = version
          @auto_update = auto_update
          @post_install = post_install
          @force = force
        end

        private

        def perform_install!(root_path)
          if version
            install_from_archive!(root_path)
          else
            install_latest!(root_path)
          end
        end
      end
    end
  end
end
