# frozen_string_literal: true

module ArchRy
  module Pacman
    # Packages in Arch Linux are built using the makepkg utility and the information stored in a PKGBUILD file. When
    # makepkg runs, it searches for a PKGBUILD in the current directory and follows the instructions in it to acquire
    # the required files and/or compile them to be packed within a package file (pkgname.pkg.tar.zst). The resulting
    # package contains binary files and installation instructions ready to be installed by pacman.
    #
    # See https://wiki.archlinux.org/title/Creating_packages
    module OwnBuild
      class Package
        attr_reader :url, :sha512, :post_install

        def initialize(url:, sha512:, post_install: nil)
          @url = url
          @sha512 = sha512
          @post_install = post_install
        end

        def install!(root_path = nil)
          download_archive!(root_path)
          verify_checksum(root_path)
          perform_install!(root_path)
          run_post_install!(root_path)
          delete_archive!(root_path)
        end

        private

        def download_archive!(root_path)
          TTY::Command.default.run :curl, '--location', '--output', filepath(root_path), url, only_output_on_error: true
        end

        def verify_checksum(root_path)
          TTY::Command.default.run :bash, '-c', "echo #{sha512} #{filepath(root_path)} | sha512sum -c -"
        end

        def perform_install!(root_path)
          if root_path
            TTY::Command.default.run :pacstrap, '-U', root_path, filepath(root_path)
          else
            TTY::Command.default.run :pacman, '-U', filepath(root_path)
          end
        end

        def delete_archive!(root_path)
          TTY::Command.default.run :rm, filepath(root_path)
        end

        def filepath(root_path)
          "#{root_path}/root/#{sha512}.pkg.tar.zst"
        end

        def run_post_install!(root_path)
          Array(post_install).each do |command|
            if root_path
              TTY::Command.default.run :'arch-chroot', root_path, :bash, '-c', command
            else
              TTY::Command.default.run :bash, '-c', command
            end
          end
        end
      end
    end
  end
end
