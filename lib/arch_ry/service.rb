# frozen_string_literal: true

module ArchRy
  # systemd is a suite of basic building blocks for a Linux system. It provides a system and service manager that runs
  # as PID 1 and starts the rest of the system.
  #
  # systemd provides aggressive parallelization capabilities, uses socket and D-Bus activation for starting services,
  # offers on-demand starting of daemons, keeps track of processes using Linux control groups, maintains mount and
  # automount points, and implements an elaborate transactional dependency-based service control logic. systemd
  # supports SysV and LSB init scripts and works as a replacement for sysvinit.
  #
  # See https://wiki.archlinux.org/title/Systemd
  class Service
    def self.ntpd
      new(name: 'ntpd')
    end

    attr_reader :name

    def initialize(name:)
      @name = name
    end

    def enable!
      TTY::Command.default.run :systemctl, 'enable', name
    end
  end
end
