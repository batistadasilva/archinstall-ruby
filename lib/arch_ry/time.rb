# frozen_string_literal: true

require 'arch_ry/time/zone'
require 'arch_ry/time/hardware_clock'

module ArchRy
  # In an operating system, the time (clock) is determined by three parts: time value, whether it is local time or UTC
  # or something else, time zone, and Daylight Saving Time (DST) if applicable.
  #
  # See https://wiki.archlinux.org/title/System_time
  module Time
  end
end
