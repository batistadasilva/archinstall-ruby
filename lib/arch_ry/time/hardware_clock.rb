# frozen_string_literal: true

require 'singleton'

module ArchRy
  module Time
    # The hardware clock (a.k.a. the Real Time Clock (RTC) or CMOS clock) stores the values of: Year, Month, Day, Hour,
    # Minute, and Seconds. Only 2016, or later, UEFI firmware has the ability to store the timezone, and whether DST is
    # used.
    #
    # See https://wiki.archlinux.org/title/System_time#Hardware_clock
    class HardwareClock
      include Singleton

      # Set hardware clock from system clock
      def systohc!
        TTY::Command.default.run :hwclock, '--systohc'
      end

      # Set system clock from hardware clock
      def hctosys!
        TTY::Command.default.run :hwclock, '--hctosys'
      end
    end
  end
end
