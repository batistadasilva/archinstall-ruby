# frozen_string_literal: true

module ArchRy
  module Time
    # A time zone is an area that observes a uniform standard time for legal, commercial and social purposes. Time zones
    # tend to follow the boundaries between countries and their subdivisions instead of strictly following longitude,
    # because it is convenient for areas in frequent communication to keep the same time.
    #
    # https://en.wikipedia.org/wiki/Time_zone
    class Zone
      LOCALTIME_FILE = '/etc/localtime'
      ZONEINFO_DIR = '/usr/share/zoneinfo'

      def self.all
        TTY::Command.quiet.run(:timedatectl, 'list-timezones').map do |line|
          new(name: line)
        end
      end

      attr_reader :name

      def initialize(name:)
        @name = name
      end

      def set_as_current!
        TTY::Command.default.run :ln, '-sf', zoneinfo_path, LOCALTIME_FILE
      end

      private

      def zoneinfo_path
        "#{ZONEINFO_DIR}/#{name}"
      end
    end
  end
end
