# frozen_string_literal: true

module TTY
  module Extensions
    module Markdown
      def default
        @default ||= DefaultMarkdown
      end
    end

    module DefaultMarkdown
      extend TTY::Markdown

      def self.print(markdown)
        puts parse(markdown)
      end
    end
  end
end

TTY::Markdown.extend(TTY::Extensions::Markdown)
